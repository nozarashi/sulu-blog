module.exports = {
  extends: 'airbnb-base',
  parser: '@babel/eslint-parser',
  parserOptions: {
    ecmaVersion: 2021,
  },
  env: {
    node: true,
    es6: true,
    browser: true,
    jquery: true,
  },
  rules: {
    'object-shorthand': ['error', 'always', {
      avoidQuotes: true,
      avoidExplicitReturnArrows: true,
    }],
    'function-paren-newline': ['error', 'consistent'],
    'max-len': ['warn', 120, 2, {
      ignoreUrls: true,
      ignoreComments: false,
      ignoreRegExpLiterals: true,
      ignoreStrings: true,
      ignoreTemplateLiterals: true,
    }],
    'import/prefer-default-export': 'off',
    'class-methods-use-this': 'off',
  },
};
