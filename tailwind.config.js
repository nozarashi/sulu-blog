module.exports = {
  // mode: 'jit',
  purge: [
    './templates/**/*.html.twig',
    './assets/**/*.css',
    './assets/**/*.js',
    './assets/**/*.jsx',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        inter: "'Inter var', sans-serif"
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/aspect-ratio'),
  ],
}
