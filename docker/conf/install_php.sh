#!/bin/bash

set -xe

APCU_VERSION=5.1.20

docker-php-ext-install pdo_pgsql pgsql mbstring xml zip intl xsl exif pdo_mysql opcache
docker-php-ext-configure pgsql -with-pgsql=/usr/include/postgresql/

docker-php-ext-configure gd --with-jpeg --with-webp --with-freetype && docker-php-ext-install gd
apt -y install libzip-dev && docker-php-ext-install zip

# redis
mkdir -p /usr/src/php/ext/redis; \
	curl -fsSL https://pecl.php.net/get/redis --ipv4 | tar xvz -C "/usr/src/php/ext/redis" --strip 1; \
	docker-php-ext-install redis

# apcu
mkdir -p /usr/src/php/ext/apcu; \
	curl -fsSL https://pecl.php.net/get/apcu --ipv4 | tar xvz -C "/usr/src/php/ext/apcu" --strip 1; \
	docker-php-ext-install apcu

# amqp
apt install -y librabbitmq-dev
mkdir -p /usr/src/php/ext/amqp; \
	curl -fsSL https://pecl.php.net/get/amqp --ipv4 | tar xvz -C "/usr/src/php/ext/amqp" --strip 1; \
	docker-php-ext-install amqp
