#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

bash docker/conf/env_provision.sh
bash docker/conf/install_php.sh
