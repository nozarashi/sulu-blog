server {
    listen 80 default_server;
    listen [::]:80 default_server;
    server_name localhost;

    root /var/www/public;

    # set client body size to 25M
    client_max_body_size 25M;

    location / {
        try_files $uri /index.php$is_args$args;
    }

    # expire
        location ~* \.(?:ico|css|js|gif|webp|jpe?g|png|svg|woff|woff2|eot|ttf|mp4)$ {
            # try to serve file directly, fallback to index.php
            try_files $uri /index.php$is_args$args;
            access_log off;
            expires -1;
            add_header Pragma public;
            add_header Cache-Control "public";
        }

    location ~ ^/(index|config)\.php(/|$) {
        fastcgi_pass php:9000;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        internal;
    }

    location ~ \.php$ {
        return 404;
    }

    error_log /var/log/nginx/project_error.log;
    access_log /var/log/nginx/project_access.log;
}
