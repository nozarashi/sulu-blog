#!/bin/bash

set -xe

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "\$HASH=\`curl -sS https://composer.github.io/installer.sig\`;if (hash_file('sha384', 'composer-setup.php') === \$HASH) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php --filename=composer --install-dir=/usr/local/bin
php -r "unlink('composer-setup.php');"
