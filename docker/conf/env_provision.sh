#!/bin/bash

set -xe

apt-get -yqq update && apt-get -y install apt-transport-https \
        ca-certificates \
        curl lsb-release \
        wget \
        acl \
        build-essential \
        libcurl4-openssl-dev \
        curl \
        gnupg \
        gnupg2 \
        git \
        locales \
        libzip-dev \
        libxslt-dev \
        net-tools \
        zip \
        unzip \
        zlib1g-dev \
        libpq-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng-dev \
        libwebp-dev \
        zlib1g-dev \
        libxml2-dev \
        libzip-dev \
        libonig-dev \
        libsodium-dev \
        pkg-config
