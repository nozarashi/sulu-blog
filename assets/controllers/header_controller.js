import { Controller } from 'stimulus';
import { useClickOutside, useTransition } from 'stimulus-use';

/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="hello" attribute will cause
 * this controller to be executed. The name "hello" comes from the filename:
 * hello_controller.js -> "hello"
 *
 * Delete this file or adapt it for your use!
 */
export default class extends Controller {
  static targets = ['mobileMenu'];

  connect() {
    useClickOutside(this, { element: this.mobileMenuTarget });

    const {
      transitionEnterActive,
      transitionEnterFrom,
      transitionEnterTo,
      transitionLeaveActive,
      transitionLeaveFrom,
      transitionLeaveTo,
    } = this.mobileMenuTarget.dataset;

    useTransition(this, {
      element: this.mobileMenuTarget,
      enterActive: transitionEnterActive,
      enterFrom: transitionEnterFrom,
      enterTo: transitionEnterTo,
      leaveActive: transitionLeaveActive,
      leaveFrom: transitionLeaveFrom,
      leaveTo: transitionLeaveTo,
      hiddenClass: 'hidden',
    });

    this.element.addEventListener('keyup', (e) => {
      if (e.key === 'Escape') {
        this.close();
      }
    });
  }

  toggle() {
    console.log('toggle');
    this.toggleTransition();
  }

  open() {
    this.enter();
  }

  close() {
    this.leave();
  }
}
