.PHONY: help

.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

PHP_VERSION ?= 8.0
EXEC_ROOT       ?= docker-compose exec -T php
EXEC        ?= docker-compose exec --user=www-data -T php
PHP         = $(EXEC) php
SYMFONY     = $(PHP) bin/console
SYMFONY_TEST = $(PHP) bin/console -e test
ADMIN_CONSOLE = $(PHP) bin/adminconsole
WEBSITE_CONSOLE = $(PHP) bin/websiteconsole
COMPOSER    = $(EXEC) composer
UID         ?= 1000
GID         ?= 1000

BEHAT ?= $(PHP) ./vendor/bin/behat --format progress

DB_NAME = main

##
## Project
## -------
##

#######
.PHONY: install stop install_app version node_modules
#######

install: docker_install install_app  database permissions ## Install and start the project

stop: docker_stop ## Stop the project


install_app: install_vendor node_modules version ## Install and start the project for CI

node_modules:
	$(MAKE) --no-print-directory permissions
	$(EXEC) yarn install --force
	$(EXEC) yarn build

yarn_watch:
	$(EXEC) yarn watch

version: ## Displays Symfony version
	${SYMFONY} --version

.PHONY: database db_create migrate generate_migration
database: db_create migrate
db_create:
	$(SYMFONY) doctrine:database:drop --if-exists --force
	$(SYMFONY) doctrine:database:create --if-not-exists
	$(PHP) bin/adminconsole sulu:build dev

migrate:
	$(SYMFONY) doctrine:mi:migrate -n --allow-no-migration

generate_migration:
	$(SYMFONY) doctrine:mi:diff

load_fixtures:
	$(SYMFONY) doctrine:fixtures:load --no-interaction --append

dump_db:
	docker-compose exec db mysqldump -u main -pmain main > docker/db/dump.sql

restore_db:
	docker-compose exec db /bin/bash -c 'mysql -u main -pmain main < /home/db/dump.sql'


##
## Docker
## ------
##

#######
.PHONY: docker_install docker_build docker_compose docker_stop
#######

docker_build:
	docker-compose build --pull --build-arg UID=$$(id -u) --build-arg GID=$$(id -g) --build-arg PHP_VERSION=$(PHP_VERSION)

docker_compose: ## Compose Image docker
	docker-compose up -d

docker_stop:
	docker-compose stop

docker_down:
	docker-compose down --remove-orphans -v

docker_rmi:
	docker-compose down --remove-orphans --rmi all -v

docker_install: docker_build docker_compose ## Install docker

##
## Utils
## -----
##

shell: ## Starts a bash shell as www-data on the docker PHP machine
	docker-compose exec --user=www-data php bash

shell_db:
	docker-compose exec db mysql -h localhost -u main -pmain

shell_db_test:
	docker-compose exec db mysql -h localhost -u main -pmain_test

##
## Tests
## -----
##

fix: vendor ## Launch php-cs-fixer
	$(PHP) ./vendor/bin/php-cs-fixer fix --config=.php-cs-fixer.dist.php --allow-risky=yes

phpcs-fixer:
	$(PHP) ./vendor/bin/php-cs-fixer fix --config=.php-cs-fixer.dist.php --allow-risky=yes --dry-run -v



# rules based on files
composer.lock: ## Generate composer.lock
	$(COMPOSER) update --lock --no-scripts --no-interaction;

install_vendor: composer.lock ## Install vendor
	$(COMPOSER) install --optimize-autoloader --quiet

vendor: ##Update vendor
	$(COMPOSER) update

cache_clear:
	$(SYMFONY) cache:clear

permissions: ## Fix file permissions for project
	$(EXEC_ROOT) chown -R $$(id -u):$$(id -g) *;
	$(EXEC_ROOT) chown -R $$(id -u):$$(id -g) /home/www-data;
	$(EXEC_ROOT) chown -R $$(id -u):$$(id -g) /tmp;

test_database: SYMFONY = $(SYMFONY_TEST)
test_database: DB_NAME = main_test
test_database: database load_fixtures

tests: phpunit behat

behat:
	$(MAKE) --no-print-directory test_database
ifdef BEHAT_TAGS
	$(BEHAT) --tags=$(BEHAT_TAGS)
else
	$(BEHAT)
endif

phpqa: install_phpunit
	$(PHP) ./vendor/bin/phpqa

pre_commit: fix phpqa tests

install_phpunit: vendor
	$(PHP) bin/phpunit install

phpunit:
	$(MAKE) --no-print-directory test_database
	$(PHP) bin/phpunit

.PHONY: download_lang init_document
download_lang:
	$(SYMFONY) sulu:admin:download-language
init_document:
	$(ADMIN_CONSOLE) sulu:document:initialize

rector:
	vendor/bin/rector process

encore:
	$(EXEC) yarn dev

watch:
	$(EXEC) yarn watch

eslint:
	$(EXEC) yarn lint

fix_js:
	$(EXEC) yarn fix
